Application.run(['$rootScope', '$timeout', 'CONSTANTS', 'Utils', '$location', function($rootScope, $timeout, CONSTANTS, Utils, $location) {
    "use strict";
    /* perform any action with the variables inside this block(on-page-load) */
    var pieChart = [],
        wishList = [],
        deviceID;
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */

        $rootScope.Variables.TopGainers.dataSet.Portfolio = [];
        $rootScope.Variables.TopGainers.dataSet.Wish = [];

    };





    $rootScope.tenantId = localStorage.getItem('STOCK_TENANT') || (function(len) {

        var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';

        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    })(16);





    $rootScope.tenantInfoonSuccess = function(variable, data) {

        if (data.length === 0) {
            $rootScope.Variables.addTenantDevice.insertRecord();

        } else {

            localStorage.setItem('STOCK_TENANT', data[0].tenantid);
            $timeout(loadVariables, 100);
        }
    };



    function loadVariables() {
        $rootScope.Variables.wishListDB.update();
        $rootScope.Variables.portfolioListDB.update();

    }


    $rootScope.APP_RUN_MODE = true;
    $rootScope.wishListDBonSuccess = function(variable, data) {
        $rootScope.Variables.StockArray.setValue('Wish', getSymbolCSV(getNonEmtptyData(data.content)));

        wishList = data.content;
        console.log(wishList);
        $timeout(function() {
            if (wishList.length > 0) {

                $rootScope.Variables.symbolDataAPIWish.update();
            } else {
                $rootScope.Variables.TopGainers.dataSet.Wish = [];
            }
        });
    };

    $rootScope.portfolioListDBonSuccess = function(variable, data) {
        $rootScope.Variables.SymbolSearchDB.update();
        pieChart = getNonEmtptyData(data.content);
        $rootScope.Variables.StockArray.dataSet.Portfolio = getSymbolCSV(pieChart);
        $timeout(function() {
            // Get symbol data if there are any symbols
            if (pieChart.length > 0) {
                $rootScope.Variables.symbolDataAPIPortfolio.update();
            } else {
                $rootScope.Variables.TopGainers.dataSet.Portfolio = [];
            }
        });
    };

    function getNonEmtptyData(data) {
        return _.filter(data, function(v) {
            return !(_.isUndefined(v) || _.isNull(v));
        });
    }

    function getSymbolCSV(stockData) {;
        return _.map(stockData, function(v) {
            return '\'' + v.Symbol + '\'';
        }).join(',');
    }

    function getTopGainers(objArr) {
        var count = 0,
            arr = [];

        objArr.sort(function(a, b) {
            return b.Change - a.Change
        });

        while ((count < objArr.length) && (objArr[count].Change)) {
            if (count === 3)
                break;
            arr.push(objArr[count])
            count++;
        }

        return arr;
    }

    $rootScope.symbolDataAPIPortfolioonSuccess = function(variable, data) {
        var obj,
            arr = [],
            count, gain, loss;

        count = gain = loss = 0;
        if (!$.isArray(data.query.results.quote)) {
            arr.push(data.query.results.quote);
        } else
            arr = data.query.results.quote;
        for (var obj of pieChart) {
            if (arr.length > count) {
                obj.Change = arr[count].Change;
                obj.Ask = arr[count].Ask;
                obj.StockExchange = arr[count].StockExchange;
                if (!isNaN(obj.ShareQty) && !isNaN(obj.ShareValuePurchase) && !isNaN(obj.BrokerCommission))
                    if (obj.Ask > obj.ShareValuePurchase) {
                        gain += (obj.Ask * obj.ShareQty - obj.ShareValuePurchase * obj.ShareQty) - obj.BrokerCommission;
                    } else {
                        loss += obj.ShareValuePurchase * obj.ShareQty - obj.Ask * obj.ShareQty;
                    }
                obj.PercentChange = arr[count].PercentChange;
                count++;
            }
        }
        $rootScope.Variables.pieChartData.dataSet = [{
            "label": "Gain",
            "values": gain
        }, {
            "label": "Loss",
            "values": loss
        }];
        $rootScope.Variables.StockArray.dataSet.Portfolio = pieChart;
        pieChart = JSON.parse(JSON.stringify(pieChart));
        $rootScope.Variables.TopGainers.dataSet.Portfolio = [];
        $rootScope.Variables.TopGainers.dataSet.Portfolio = getTopGainers(pieChart);
    };



    $rootScope.deviceInfoonSuccess = function(variable, data) {
        if (CONSTANTS.hasCordova && Utils.isIphone()) {
            /*
             * IPHONE UUID is not same across app upgrades or reinstalls.
             * So, a plugin is used in case of iphone to generate a UUID which might not
             * be the same UUID that IOS assigns to this app.
             * This logic will be removed when user signup is supported.
             */
            window.plugins.uniqueDeviceID.get(function(uuid) {
                $rootScope.Variables.deviceInfo.dataSet.deviceUUID = uuid;
                $timeout(function() {
                    $rootScope.Variables.tenantInfo.update();
                }, 1000);
            });
        } else {
            $rootScope.Variables.tenantInfo.setFilter('deviceuuid', $rootScope.Variables.deviceInfo.dataSet.deviceUUID);

            $rootScope.Variables.tenantInfo.update();

        }
    };

    $rootScope.symbolDataAPIWishonSuccess = function(variable, data) {
        var count = 0;
        $rootScope.Variables.StockArray.setValue('Wish', []);
        var arr = [];
        if (!$.isArray(data.query.results.quote)) {
            arr.push(data.query.results.quote);
        } else
            arr = data.query.results.quote;
        for (var obj of wishList) {
            if (arr.length > count) {
                obj.Change = arr[count].Change;
                obj.Ask = arr[count].Ask;
                obj.StockExchange = arr[count].StockExchange;
                obj.PercentChange = arr[count].PercentChange;
                count++;
            }
        }
        $rootScope.Variables.StockArray.setValue('Wish', wishList);

        var wishArr = JSON.parse(JSON.stringify(wishList));

        $rootScope.Variables.TopGainers.dataSet.Wish = [];
        console.log(wishArr);
        $rootScope.Variables.TopGainers.dataSet.Wish = getTopGainers(wishArr);
    };
}]);

//TODO in Wavemaker : Provide a variable to show or hide spinner.
Application.factory('wmSpinner', function($q, $rootScope, $log) {
    var spinnerCount = 0;

    function showSpinner() {
        spinnerCount++;
        WM.element('body >.app-spinner:first').removeClass('ng-hide');
        WM.element('body >.app-spinner:first i').removeClass('ng-hide');
    }

    function hideSpinner(force) {
        spinnerCount--;
        if (force || spinnerCount < 0) {
            spinnerCount = 0;
        }
        if (spinnerCount === 0) {
            WM.element('body >.app-spinner:first').addClass('ng-hide');

        }
    }

    return {
        show: showSpinner,
        hide: hideSpinner,
        request: function(config) {
            showSpinner();
            return config || $q.when(config);
        },
        response: function(response) {
            hideSpinner();
            return response || $q.when(response);

        },
        responseError: function(response) {
            hideSpinner(true);
            return $q.reject(response);
        }
    };
});
Application.config(function($httpProvider) {
    $httpProvider.interceptors.push('wmSpinner');

});