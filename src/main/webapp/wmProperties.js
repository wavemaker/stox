var _WM_APP_PROPERTIES = {
  "activeTheme" : "stock-tracker",
  "defaultLanguage" : "en",
  "displayName" : "Stox",
  "homePage" : "Main",
  "name" : "Stox",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.1"
};