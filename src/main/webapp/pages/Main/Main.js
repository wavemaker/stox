Application.$controller("MainPageController", ["$scope", "$filter", "$rootScope", "$location", function($scope, $filter, $rootScope, $location) {
    "use strict";
    var pieChart = [];
    var count = 0;

    // TODO in Wavemaker  BackButton exits the Application

    document.addEventListener("backbutton", onBackKeyDown, false);

    function onBackKeyDown() {
        // Handle the back button

        if (window.location.hash == "#/Main" || window.location.hash == "#/Main.MyPortfolio" || window.location.hash == "#/Main.MyWishList") {

            if (navigator) {
                navigator.Backbutton.goHome(function() {

                    console.log('success')
                }, function() {
                    console.log('fail')
                });
            }
        } else {
            window.history.go(-1);
        }


    }



    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {

        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        initDate();

    };

    function initDate() {
        var selectedDate = new Date();
        selectedDate.setDate(selectedDate.getDate() - 10);
        $scope.Variables.selectedDate.dataSet.value = $filter('date')(selectedDate, 'yyyy-MM-dd');
        selectedDate = new Date();
        selectedDate.setDate(selectedDate.getDate() - 400);
        $scope.Variables.selectedDate.dataSet.dataValue = $filter('date')(selectedDate, 'yyyy-MM-dd');
        $scope.Variables.currentDate.dataSet.dataValue = $filter('date')(new Date(), 'yyyy-MM-dd');
    }

    $scope.segmentControlBeforeSegmentChange = function($isolateScope, $old, $new) {
        if ($new === 0) {
            $scope.Widgets.mobile_navbar1.title = "DASHBOARD";
        } else if ($new === 1) {
            $scope.Widgets.mobile_navbar1.title = "MY PORTFOLIO";
        } else if ($new === 2) {
            $scope.Widgets.mobile_navbar1.title = "WISHLIST";
        }
    };

    function navigate($isolateScope) {
        var symbol = $isolateScope.item.Symbol;
        $scope.Variables.Symbol.dataSet.dataValue = symbol;
        $scope.Variables.Symbol.dataSet.sid = $isolateScope.item.SID;
        $scope.Variables.compareSymbols.dataSet.firstSymbol = symbol;
        $scope.Variables.StateObject.dataSet.stockDataPageState = "2";
    }


    $scope.portfolioTap = function($event, $isolateScope) {
        navigate($isolateScope);
        $scope.Variables.StateObject.dataSet.stockDataType = "portfolio";
    };


    $scope.WishListTap = function($event, $isolateScope) {
        navigate($isolateScope);
        $scope.Variables.StateObject.dataSet.stockDataType = "wish";
    };


    $scope.topPortfolioTap = function($event, $isolateScope) {
        navigate($isolateScope);
        $scope.Variables.StateObject.dataSet.stockDataType = "portfolio";
    };


    $scope.topWishTap = function($event, $isolateScope) {
        navigate($isolateScope);
        $scope.Variables.StateObject.dataSet.stockDataType = "wish";
    };


    $scope.SearchListTap = function($event, $isolateScope) {
        if ($isolateScope.item.exist === 0) {
            $scope.Variables.Symbol.dataSet.dataValue = $scope.Widgets.SearchList.selecteditem.Symbol;

            $scope.Variables.StateObject.dataSet.stockDataPageState = "1";
            $scope.Variables.navigateToStockData.navigate();
        } else {
            $scope.Variables.toasterExist.notify();
        }
    };


    $scope.addPortfolioTap = function($event, $isolateScope) {

        $scope.Widgets.mobile_navbar1.showSearchbar = true;
    };


    $scope.addWishlistTap = function($event, $isolateScope) {

        $scope.Widgets.mobile_navbar1.showSearchbar = true;
    };


    $scope.anchor1Tap = function($event, $isolateScope) {
        var selectedItems = $scope.Widgets.WishList.selecteditem;
        $scope.Variables.compareSymbols.dataSet.firstSymbol = selectedItems[0].Symbol;
        $scope.Variables.compareSymbols.dataSet.secondSymbol = selectedItems[1].Symbol;
    };




}]);