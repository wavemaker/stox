Application.$controller("WishListPageController", ["$scope", function($scope) {
    "use strict";
    var symbols = [];
    var count = 1;
    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        symbols.push($scope.Variables.compareSymbols.dataSet.firstSymbol);
        $scope.Widgets.mobileNav.title = symbols[0] + " Vs ";
    };


    $scope.wishListClick = function($event, $isolateScope) {
        if (count <= 1) {
            symbols.push($isolateScope.item.Symbol);
            count++;
            if (count === 2) {
                $scope.Variables.compareSymbols.dataSet.firstSymbol = symbols[0];
                $scope.Variables.compareSymbols.dataSet.secondSymbol = symbols[1];
                setTitle(symbols[0] + " Vs " + symbols[1]);
                showComparebtn();
            }
        } else {
            symbols[1] = $isolateScope.item.Symbol;
            $scope.Variables.compareSymbols.dataSet.secondSymbol = symbols[1];
            setTitle(symbols[0] + " Vs " + symbols[1]);
            showComparebtn();
        }
    };

    function setTitle(title) {
        $scope.Widgets.mobileNav.title = title;
    }

    function showComparebtn() {
        if (symbols[0] !== symbols[1])
            $scope.Widgets.compareBtn.show = true;
        else
            $scope.Widgets.compareBtn.show = false;
    }

}]);