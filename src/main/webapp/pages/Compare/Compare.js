Application.$controller("ComparePageController", ["$scope", "$filter", function($scope, $filter) {
    "use strict";
    var stock = [];
    var days = 6;

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
        $('[name="weekOne"]').addClass("active");

    };


    $scope.firstSymbolGraphonSuccess = function(variable, data) {
        var val;
        debugger;
        for (var i = 0; i < data.query.results.quote.length; i++) {
            var obj = {
                date: data.query.results.quote[i].Date,
                openFirst: data.query.results.quote[i].Open,

                closeFiirst: data.query.results.quote[i].Close
            }
            stock.push(obj);
        }
        $scope.Variables.secondSymbolGraph.update({}, function(data) {
            var len = stock.length <= data.query.results.quote.length ? stock.length : data.query.results.quote.length;
            for (var i = 0; i < len; i++) {
                stock[i].openSecond = data.query.results.quote[i].Open;
                stock[i].closeSecond = data.query.results.quote[i].Close;
            }
            $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
            $scope.Variables.globalSpinner.dataSet.dataValue = false;
        });
    };


    $scope.compareSymbolsDataonSuccess = function(variable, data) {
        $scope.Variables.symbolData.dataSet.query = data;
    };


    $scope.yearTap = function($event, $isolateScope) {
        makeActive($event);
        days = 276;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };


    $scope.halfYearTap = function($event, $isolateScope) {
        makeActive($event);
        days = 125;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };


    $scope.monthThreeTap = function($event, $isolateScope) {
        makeActive($event);
        days = 60;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };


    $scope.monthOneTap = function($event, $isolateScope) {
        makeActive($event);
        days = 20;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };


    $scope.weekTwoTap = function($event, $isolateScope) {
        makeActive($event);
        days = 11;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };


    $scope.weekOneTap = function($event, $isolateScope) {
        makeActive($event);
        days = 6;
        $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
    };

    function makeActive($event) {
        WM.element($event.currentTarget).parent().children().removeClass('active');
        WM.element($event.currentTarget).addClass('active')
    }

    $scope.firstSymbolGraphFullonSuccess = function(variable, data) {
        var val;
        var stockTemp = [];
        debugger;
        for (var i = 0; i < data.query.results.quote.length; i++) {
            var obj = {
                date: data.query.results.quote[i].Date,
                openFirst: data.query.results.quote[i].Open,

                closeFiirst: data.query.results.quote[i].Close
            }
            stockTemp.push(obj);
        }
        $scope.Variables.secondSymbolGraphFull.update({}, function(data) {
            var len = stockTemp.length <= data.query.results.quote.length ? stockTemp.length : data.query.results.quote.length;
            for (var i = 0; i < len; i++) {
                stockTemp[i].openSecond = data.query.results.quote[i].Open;
                stockTemp[i].closeSecond = data.query.results.quote[i].Close;
            }
            stock = stockTemp;
            $scope.Variables.compareGraphData.dataSet = stock.slice().splice(0, days);
            $scope.Variables.globalSpinner.dataSet.dataValue = false;
        });
    };

}]);