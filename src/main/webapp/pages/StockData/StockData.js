Application.$controller("StockDataPageController", ["$scope", "$filter", "$rootScope", "$timeout", function($scope, $filter, $rootScope, $timeout) {
    "use strict";
    var graphData = [];
    var counter = 0;
    var temp = 6;

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */

    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */

        $('[name="weekOne"]').addClass("active");
    };

    function duplicate(data, time) {
        return data;
    }

    $scope.selectMenuSelect = function($event, $isolateScope, $item) {
        if ($item === 'Add To Wish List') {
            counter = 0;
            $scope.Variables.insertWishList.insertRecord({}, function() {
                $scope.Variables.wishListDB.update({}, function(data) {
                    reloadWishList(data)
                });
            });
            $scope.Variables.globalSpinner.dataSet.dataValue = true;
        } else if ($item === 'Add To Portfolio') {
            $scope.Variables.SymbolObject.dataSet.Name = $scope.Variables.stockData.dataSet.query.results.quote.Name;
            $scope.Variables.SymbolObject.dataSet.symbol = $scope.Variables.stockData.dataSet.query.results.quote.Symbol;
            $scope.Variables.SymbolObject.dataSet.StockExchange = $scope.Variables.stockData.dataSet.query.results.quote.StockExchange;
            $scope.Variables.navigateToAddPortfolio.navigate();

        }
    };

    function reloadWishList(data) {
        $timeout(function() {
            $scope.Variables.symbolDataAPIWish.update({}, function(data) {
                $scope.Variables.movetoWishList.navigate();
            }, function(data) {
                $scope.Variables.movetoWishList.navigate();
            })
        });
    }

    $scope.StockGraphonSuccess = function(variable, data) {
        graphData = data.query.results.quote;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, temp);
        $scope.Variables.globalSpinner.dataSet.dataValue = false;
    };

    $scope.stockDataonSuccess = function(variable, data) {
        $scope.Variables.symbolData.dataSet = data;
    };

    $scope.compareSelect = function($event, $isolateScope, $item) {
        if ($item === 'Compare') {
            $scope.Variables.navigateToWishList.navigate();
        } else
        if ($item === 'Delete') {
            $scope.Variables.DeleteSymbol.setInput('StockId', $scope.Variables.Symbol.dataSet.sid);
            $scope.Variables.DeleteSymbol.update({}, function(data) {
                if ($scope.Variables.StateObject.dataSet.stockDataType === 'wish')
                    $scope.Variables.wishListDB.update({}, function(data) {
                        $scope.Variables.movetoWishList.navigate();
                    });
                else
                if ($scope.Variables.StateObject.dataSet.stockDataType === 'portfolio')
                    $scope.Variables.portfolioListDB.update({}, function(data) {
                        $scope.Variables.navigateToPortfolio.navigate();
                    });

            });

        }
    };


    $scope.stockGraphFullonSuccess = function(variable, data) {
        graphData = data.query.results.quote;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, temp);
    };

    $scope.weekOneTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 6;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 6);
    };


    $scope.weekTwoTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 12;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 12);
    };


    $scope.monthOneTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 20;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 20);
    };


    $scope.monthThreeTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 60;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 60);
    };


    $scope.halfYearTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 125;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 125);
    };


    $scope.yearTap = function($event, $isolateScope) {
        makeActive($event);
        temp = 276;
        $scope.Variables.graphData.dataSet = graphData.slice().splice(0, 276);
    };

    function makeActive($event) {
        WM.element($event.currentTarget).parent().children().removeClass('active');
        WM.element($event.currentTarget).addClass('active')
    }

}]);